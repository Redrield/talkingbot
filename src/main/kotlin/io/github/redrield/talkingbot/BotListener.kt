package io.github.redrield.talkingbot

import org.bukkit.Bukkit
import org.bukkit.ChatColor
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.AsyncPlayerChatEvent
import org.bukkit.event.player.PlayerJoinEvent
import org.bukkit.event.player.PlayerQuitEvent
import java.util.*

class BotListener : Listener {

    private val plugin: TalkingBot;

    constructor(plugin: TalkingBot) {
        this.plugin = plugin
    }

    @EventHandler
    fun onPlayerChat(e: AsyncPlayerChatEvent) {
        val prefix = ChatColor.translateAlternateColorCodes('&', plugin.config.getString("bot-prefix"))
        val msg = e.message
        val p = e.player

        var conf = plugin.config.getConfigurationSection("special-match")
        var split = msg.split(" ")
        for(s in split) {
            e.isCancelled = conf.getBoolean(s + ".block")
            Bukkit.broadcastMessage(prefix + ChatColor.translateAlternateColorCodes('&', conf.getString(s + ".response")))
        }

        var lookNew = StringBuilder()
        if(plugin.toggleState[p] ?: false || !p.hasPermission("talkingbot.interact")) {
            return
        }
        for(i in 0..split.size) {
            if(i == 0) {
                lookNew.append(split[i])
            }
            lookNew.append("-").append(split[i])
            if(plugin.config.contains("miscellaneous." + lookNew.toString().toLowerCase())) {
                val sb = StringBuilder()
                for(j in i+1..split.size)
                    sb.append(if(j!=i+1) " " else "")
                var match = false
                for(s in plugin.config.getStringList("ignore-words")) {
                    if(sb.toString().replace("[\\?\\.!]+", "").equals(s, true)) {
                        match = true
                        break
                    }
                }
                if(match)
                    break

            }
        }
        if(plugin.config.contains("miscellaneous." + lookNew.toString().toLowerCase())) {
            val selection = plugin.config.getStringList("miscellaneous." + lookNew.toString().toLowerCase())
            val say = ChatColor.translateAlternateColorCodes('&', selection.get(Random().nextInt(selection.size))).replace("%player%", p.name);
            plugin.server.scheduler.scheduleSyncDelayedTask(plugin, {
                Bukkit.broadcastMessage(prefix + " " + say)
            }, plugin.config.getLong("response-speed"))
            return
        }
        if(split.size > 0 && split[0].equals(plugin.config.getString("bot-name"), true)) {
            var lookup = msg.substring(plugin.config.getString("bot-name").length).split(" ")
            if(lookup.size == 1) {
                plugin.server.scheduler.scheduleSyncDelayedTask(plugin, {
                    val selection = plugin.config.getStringList("no-match.bot-name-only")
                    var say = selection[Random().nextInt(selection.size)].replace("%player%", p.name)
                    say = ChatColor.translateAlternateColorCodes('&', say)
                    Bukkit.broadcastMessage(prefix + " " + say)
                }, plugin.config.getLong("response-speed"))
                return
            }

            var look = "sayings"
            var sb = StringBuilder()
            for(i in 1..lookup.size) {
                look += "." + lookup[i]
                if(plugin.config.contains(look)) {
                    for(j in i+1..lookup.size) {
                        sb.append(if(j != i+1) " " else "").append(lookup[j])
                    }
                    if(plugin.config.getStringList("ignore-words").contains(sb.toString())) {
                        break
                    }
                    sb = StringBuilder()
                }
                if(plugin.config.contains(look)) {
                    val selection = plugin.config.getStringList(look)
                    val say = ChatColor.translateAlternateColorCodes('&', selection[Random().nextInt(selection.size)]).replace("%player%", p.name)
                    plugin.server.scheduler.scheduleSyncDelayedTask(plugin, {
                        Bukkit.broadcastMessage(prefix + " " + say)
                    }, plugin.config.getLong("response-speed"))
                }else {
                    plugin.server.scheduler.scheduleSyncDelayedTask(plugin, {
                        val selection = plugin.config.getStringList("no-match.question-not-found")
                        val say = ChatColor.translateAlternateColorCodes('&', selection[Random().nextInt(selection.size)]).replace("%player%", p.name)
                        Bukkit.broadcastMessage(prefix + " " + say)
                    }, plugin.config.getLong("response-speed"))
                }
            }
        }
    }

    @EventHandler
    fun onPlayerJoin(e: PlayerJoinEvent) {
        plugin.toggleState.put(e.player, false)
    }

    @EventHandler
    fun onPlayerLeave(e: PlayerQuitEvent) {
        plugin.toggleState.remove(e.player)
    }
}