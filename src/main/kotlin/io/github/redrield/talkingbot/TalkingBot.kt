package io.github.redrield.talkingbot

import org.bukkit.entity.Player
import org.bukkit.plugin.java.JavaPlugin
import java.util.*


class TalkingBot : JavaPlugin() {

    val toggleState = HashMap<Player, Boolean>()

    override fun onEnable() {
        saveDefaultConfig()
        getCommand("talkingbot").executor = BotCommands(this)
        server.pluginManager.registerEvents(BotListener(this), this)
    }
}