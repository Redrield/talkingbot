package io.github.redrield.talkingbot

import org.bukkit.ChatColor
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import java.util.*

class BotCommands : CommandExecutor {
    private val plugin: TalkingBot

    constructor(plugin: TalkingBot) {
        this.plugin = plugin
    }

    override fun onCommand(cs: CommandSender, cmd: Command, label: String, args: Array<String>) : Boolean {
        if(cs is Player) {
            if(args.size == 0) {
                cs.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.config.getString("incorrect-usage")));
                return true
            }

            when(args.size) {
                0 -> cs.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.config.getString("incorrect-usage")));
                1 -> {
                    if(args[0].equals("reload", true)) {
                        if(cs.hasPermission("talkingbot.reload")) {
                            plugin.reloadConfig()
                            cs.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.config.getString("reloaded")))
                        }else {
                            cs.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.config.getString("no-permission")))
                        }
                    }
                    when(args[0].toLowerCase()) {
                        "reload" -> {
                            if(cs.hasPermission("talkingbot.reload")) {
                                plugin.reloadConfig()
                                cs.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.config.getString("reloaded")))
                                return true
                            }else {
                                cs.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.config.getString("no-permission")))
                                return true
                            }
                        }
                        "toggle" -> {
                            if(cs.hasPermission("talkingbot.toggle")) {
                                var old = plugin.toggleState[cs] ?: false
                                plugin.toggleState.put(cs, !old)
                                cs.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.config.getString("toggle-" + (if(old) "on" else "off"))))
                                return true
                            }else {
                                cs.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.config.getString("no-permission")))
                                return true
                            }
                        }
                    }
                }
                in 2..Int.MAX_VALUE -> {
                    when(args[0].toLowerCase()) {
                        "add" -> {
                            if(cs.hasPermission("talkingbot.add")) {
                                var msg = args[1]
                                var res = ""
                                for(i in 2..args.size) {
                                    res += args[i] + " "
                                }
                                if(plugin.config.contains("miscellaneous." + msg)) {
                                    val currentResponses = plugin.config.getStringList("miscellaneous." + msg)
                                    currentResponses.add(res)
                                    plugin.config.set("miscellaneous." + msg, currentResponses)
                                    cs.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.config.getString("message-added")))
                                    plugin.saveConfig()
                                    plugin.reloadConfig()
                                    return true
                                }
                                val start = ArrayList<String>()
                                start.add(res)
                                plugin.config.set("miscellaneous." + msg, start)
                                cs.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.config.getString("message-added")))
                                return true
                            }
                            cs.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.config.getString("no-permission")))
                        }
                        else -> {
                            cs.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.config.getString("invalid-usage")))
                        }
                    }
                }
            }
        }
        return false
    }
}
